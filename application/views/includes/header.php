<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sistem Penunjang Keputusan</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/vendor/fontawesome/css/all.css" rel="stylesheet">
  <!-- styleku -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body style="background-color:#f5f5f0;">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">SPK</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('Dashboard/home');?>">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('Kriteria/home');?>">Kriteria</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('Nilai/home');?>">Nilai</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Hasil</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
