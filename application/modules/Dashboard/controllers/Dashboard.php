<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  function __construct(){
    parent::__construct();
    if($this->session->userdata("is_login") == FALSE) {
        redirect("Login");
    } elseif($this->session->userdata('level') !='1') {
      redirect("Login");
    }
}

  function home() {

    $data['title'] = 'Halaman Admin';
    $data['main_content'] = 'v_dashboard';
    $this->load->view('includes/template',$data);

  }

}
