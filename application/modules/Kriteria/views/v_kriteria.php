<!-- Page Content -->
<div class="container">
<br/>
  <div class="row">
    <div class="col-lg-12">
      <a id="tambah" class="btn-small waves-effect waves-light blue"><i class="fa fa-plus"></i> add	</a>
	  <a id="generate" class="btn-small waves-effect waves-light success"><i class="fa fa-procedures"></i> generate</a><br/>
	  <div id="load"></div>
					<div id="informasi"></div>
      <p class="lead">Daftar Kriteria</p>
      <table id='tbl' class="table table-striped table-hover">
        <thead style="background-color:#0066ff; color:white;">
          <th>ID Kriteria</th>
          <th>Nama Kriteria</th>
          <th>Action</th>
        </thead>
		<tbody id="mytable">
		</tbody>
      </table>
    </div>
  </div>
</div>

<!-- Menu Tambah -->
<form id="formadd" action="#" method="POST">
	<div class="modal fade" id="ModalAdd">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					

					<div class="modal-body">
						<div class="form-group">
							<input type="text" name="nama" class="form-control" placeholder="NAMA" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
					<button id="save" type="button" class="btn btn-primary">Simpan Perubahan</button>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</form>
<!-- /.modal -->

<!-- Menu Tambah -->
<form id="formedit" action="#" method="POST">
	<div class="modal fade" id="ModalEdit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					

					<div class="modal-body">
						<div class="form-group">
							<input type="text" name="id" class="form-control" placeholder="NAMA" hidden>
						</div>
						<div class="form-group">
							<input type="text" name="nama" class="form-control" placeholder="NAMA" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
					<button id="edit" type="button" class="btn btn-primary">Simpan Perubahan</button>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</form>
<!-- /.modal -->

<!-- MODAL DELETE -->
<form id="formdelete" action="#" method="POST">
	<div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
					
				</div>
				<div class="modal-body">
					<input type="hidden" name="id" class="form-control" required>
					<strong>Are you sure to delete this record?</strong>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					<button id="delete" type="button" class="btn btn-success">Yes</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- Bootstrap core JavaScript -->
<script src="<?= base_url();?>assets/vendor/jquery/jquery-3.4.1.min.js"></script>
<script src="<?= base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>

load();
	function load(){ 
		$.ajax({
			url : "<?php echo site_url('Kriteria/get');?>",
			type : "POST",
			dataType : "JSON",
			
			success: function(response){
				if(response.status == "0") {
					$("#mytable").append("<td colspan='3'>Tidak Ada data di database</td>")
				} else {
					$.each(response, function(i, val){
						$("#mytable").append("<tr data-id='"+ val.id +"' data-nama='"+ val.nama +"'><td>" + val.id + "</td> <td>" + val.nama + "</td><td><button id='edit' class='btn-floating btn-small waves-effect waves-light yellow darken-4'><i class='fa fa-edit'></i></button> | <button id='del' class='btn-floating btn-small waves-effect waves-light red darken-4'><i class='fa fa-trash'></i></button></td></tr>");
					})
				}
			}
		})
	}
	
	$("#tambah").on("click", function(){
			$("#ModalAdd").modal('show');
        	$('[name="nama"]').val("");
		})
		
		
	$("#save").on("click", function(){
		save();
	})
	
	function save(){
		var url = "<?php echo site_url('Kriteria/get');?>";
		$.ajax({
			url : "<?php echo site_url('Kriteria/save');?>",
			data : $('#formadd').serialize(),
			type : "POST",
			dataType : "JSON",
				
			success:function(response){
				alert(response.status);
				 $("#mytable").load(" #mytable");
				 load();
			}
		})
	}
	
	$("#mytable").on("click","#edit", function(){
		var id = $(this).parents("tr").attr("data-id");
		var nama = $(this).parents("tr").attr("data-nama")
		$("#ModalEdit").modal("show");
		$('[name="id"]').val(id);
		$('[name="nama"]').val(nama);
		
		
	})
	
	$("#edit").on("click", function(){
		edit();
	})
	
	function edit(){
		$.ajax({
			url : "<?php echo site_url('Kriteria/edit');?>",
			data : $('#formedit').serialize(),
			type : "POST",
			dataType : "JSON",
				
			success:function(response){
				alert(response.status);
				$("#mytable").load(" #mytable");
				 load();
			}
		})
	}
	
	$("#mytable").on("click","#del", function(){
		var id = $(this).parents("tr").attr("data-id");
		var nama = $(this).parents("tr").attr("data-nama")
		$("#ModalDelete").modal("show");
		$('[name="id"]').val(id);				
	})
	
	//function delete data
	$("#delete").on("click", function(){
		del();
	});
	function del(){
		$.ajax({
			url : "<?php echo site_url('Kriteria/delete');?>",
			data : $("#formdelete").serialize(),
			type : "POST",
			dataType : "JSON",

			success: function(response){
				$('#ModalDelete').modal('hide');
				alert(response.status);
				$("#mytable").load(" #mytable");
				 load();
			}
		});
	}

	$("#generate").on("click", function(){
		$("#load").html("laoding....");
		$.ajax({
			url : "<?php echo site_url('Kriteria/generate');?>",
			dataType : "JSON",

			success:function(response){
				$("#informasi").html("<p class='alert alert-success'>"+ response.status +"</p>");
				$("#load").hide();
			}
		})
	})
	
</script>
