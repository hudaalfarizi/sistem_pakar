<?php

class KriteriaModel extends CI_Model {
		
		
		function getAll(){
		    return	$this->db->get('kriteria');
		}
		
		function next_id(){
			$this->db->select_max('id');
			return $this->db->get('Kriteria');
		}
		
		function savedata($data){
			return $this->db->insert('Kriteria',$data);
		}
		
		function edit($id,$nama){
			$this->db->set("nama",$nama);
			$this->db->where("id",$id);
			$this->db->update("kriteria");
			return TRUE;
		}
		function del($id){
			$this->db->where("id",$id);
			$this->db->delete("kriteria");
			return TRUE;
		}

		function genrt(){
			$start = 1;
			$noUrut = 1;
			$char = 'CR';
			$kodekriteria = $char . sprintf("%03s", $noUrut);
			$kolom = $this->db->get('kriteria')->num_rows();
			$data = $this->db->get('kriteria')->result_array();
			for($i=1;$i<=$kolom;$i++){
				foreach($data as $row){
						$data = array(
							"baris" => $kodekriteria,
							"kolom" => $row["id"]
							);
						$this->db->insert("bobot",$data);
				}
				$kodekriteria++;
				$data = $this->db->get('kriteria')->result_array();
			}
			$dtbo = $this->db->get("bobot");
			foreach($dtbo as $row){
				$sql = 'UPDATE bobot set nilai="1" WHERE kolom = baris';
				$this->db->query($sql);
			}
		}
	}
;?>