<?php defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria extends CI_Controller {

  function __construct(){
		parent::__construct();
		if($this->session->userdata("is_login") == FALSE) {
				redirect("Login");
		} elseif($this->session->userdata('level') !='1') {
			redirect("Login");
		}
	
	$this->load->model('KriteriaModel');
	}

  function home() {

		$data['title'] = 'Halaman Admin';
		$data['main_content'] = 'v_kriteria';
		$this->load->view('includes/template',$data);

	}
  
  function get(){
	  $result = $this->KriteriaModel->getAll();
	  if($result->num_rows() == 0){
		  $response = array("status" => "0");
		  echo json_encode($response);
		} else {
		  $response = $this->KriteriaModel->getAll()->result_array();
		  echo json_encode($response);
		}
	  
	}
  
  function save(){
	  //terima inputan dari user
	  $nama = htmlspecialchars($this->input->post('nama'));
	  
	  //ambil data terakhir dari database
	  
	  $result = $this->KriteriaModel->next_id();
	  $result_array = $this->KriteriaModel->next_id()->result_array();
	  
	  //jika di database belum ada data
	  if($result->num_rows() == 0) {
		  $noUrut = 1;
		  $char = 'CR';
		  $kodekriteria = $char . sprintf("%03s", $noUrut);
		  $data = array(
			"id" => $kodekriteria,
			"nama" => $nama
		  );
		  
		  $simpan = $this->KriteriaModel->savedata($data);
		  if($simpan){
			  $response = array("status"=>"data berhasil di simpan");
			  echo json_encode($response);
			  exit();
			} else {
			  $response = array("status"=>"data gagal di simpan");
			  echo json_encode($response);
			  exit();
			}
		} else {
		  $noUrut = (int) substr($result_array[0]['id'], 3, 3);
		  $noUrut++;
		  $char = 'CR';
		  $kodekriteria = $char . sprintf("%03s", $noUrut);
		  $data = array(
			"id" => $kodekriteria,
			"nama" => $nama
		  );
		  $simpan = $this->KriteriaModel->savedata($data);
		  if($simpan){
			  $response = array("status"=>"data berhasil di simpan");
			  echo json_encode($response);
			  exit();
			} else {
			  $response = array("status"=>"data gagal di simpan");
			  echo json_encode($response);
			  exit();
			}		
		}
	}
	
	function edit(){
		$id = htmlspecialchars($this->input->post("id"));
		$nama = htmlspecialchars($this->input->post("nama"));
		
		$result = $this->KriteriaModel->edit($id,$nama);
		if($result){
			$response = array("status" => "Berhasil Edit data");
			echo json_encode($response);
			exit();
		} else {
			$response = array("status" => "Gagal Edit Data");
			echo json_encode($response);
		}
	}
	
	function delete(){
		$id = htmlspecialchars($this->input->post("id"));
		$cek_max = $this->KriteriaModel->next_id()->result_array();
		
		if($id != $cek_max[0]["id"]){
			
			$response = array("status" => "tidak bisa hapus data ini karena bukan Kriteria terakhir.");
			echo json_encode($response);
			exit();
			
		} else {
			$result = $this->KriteriaModel->del($id);
			if($result){
				$response = array("status" => "Berhasil Delete data");
				echo json_encode($response);
				exit();
			} else {
				$response = array("status" => "Gagal Delete Data");
				echo json_encode($response);
			}
		}
	}

	function generate(){
		$cek = $this->db->get("bobot")->num_rows();
		if($cek > 0){
			$response = array("status" => "sudah pernah generate data matriks. silahkan hapus terlebih dahulu");
			echo json_encode($response);
		} else {
			$save = $this->KriteriaModel->genrt();
			$response = array("status" => "berhasil generate data");
				echo json_encode($response);
		}
		
	}

}
