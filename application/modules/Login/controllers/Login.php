<?php defined("BASEPATH") or exit ('No direct script Access Allowed');

class Login extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('LoginModel');
    }
    function index(){
        if($this->session->userdata('level') =='1') {
        redirect("Dashboard/home");
        }
        elseif($this->session->userdata('level') =='2') {
        redirect("information/index");
        }
        $this->load->view('v_login');
    }

    function check_validation(){
        $nik = htmlspecialchars($this->input->post('nik'));
        $password = sha1($this->input->post('password'));

        //cek apakah user ada
        $cek = $this->LoginModel->checkUser($nik,$password)->num_rows();
        $cek_u = $this->LoginModel->checkUser($nik,$password)->result_array();
        if($cek > 0) {
            //session
            $session = array(
                "browser" => $this->agent->browser(),
                "browser_versi" =>$this->agent->version(),
                "os" => $this->agent->platform(),
                "ip" => $this->input->ip_address(),
                "url_admin" => site_url("Dashboard/home"),
                "url_siswa" => site_url("Information/index"),
                "status" => "Anda Berhasil Login",
                "nik" => $nik,
                "status" => "oke",
                "level" => $cek_u[0]['LEVEL'],
                "is_login" => true
            );
            $this->session->set_userdata($session);
            echo json_encode($session);
        } else {
            $response = array(
                "status" => "gagal",
            );
            echo json_encode($response);
        }
    }
    function keluar(){
        $this->session->sess_destroy();
        redirect('Login');
    }
}
