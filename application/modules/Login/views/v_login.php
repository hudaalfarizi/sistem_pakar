<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="aplikasi spk">
  <meta name="author" content="huda">

  <title>Welcome</title>

  <!-- Bootstrap core CSS -->
  <link href="<?= base_url();?>assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <form id='formlogin' action="#" method="POST">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">SPK</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <form id='formlogin' action="#" method="POST">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <input class='' type='text' name='nik' placeholder='NIK'>
            <span class="sr-only">(current)</span>
          </li>
          <li class="nav-item">
            <input class='' type='password' name='password' placeholder='Password'>
          </li>
          <li class="nav-item">
            <button id="sub" type="button" class='btn btn-primary btn-sm'>Login</button>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</form>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5">Selamat Datang Di Sistem Penunjang Keputusan</h1>
        <p class="lead">Silahkan Masuk dan Mulai</p>
        <ul class="list-unstyled">
          <li>Huda Alfarizi</li>
          <li>SPK V.1</li>
          <div id="load"></div>
					<div id="informasi"></div>
        </ul>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?= base_url();?>assets/vendor/jquery/jquery-3.4.1.min.js"></script>
  <script src="<?= base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script>
    $("#sub").on("click", function(){
      $("#load").html("laoding....");
      $.ajax({
          url : "<?php echo site_url('Login/check_validation');?>",
          data : $("#formlogin").serialize(),
          method : "POST",
          dataType : "JSON",

          success:function(response){
            if(response.status == "oke"){
                	$("#load").hide();
                	$("#informasi").html("<p class='alert alert-success'>Anda Berhasil Login</p>");
									if(response.level == '1') {
										window.location.href = response.url_admin;
									}
									if(response.level == '2') {
										window.location.href = response.url_siswa;
									}
            } else if(response.status =="gagal"){
                   		$("#load").hide();
                		$("#informasi").html("<p class='alert alert-danger'>NIK dan Password Salah</p>");
                		exit();
            		}
          }
        })
    })

  </script>

</body>

</html>
