<?php defined("BASEPATH") or exit("No direct script allowed");

class Nilai extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model("NilaiModel");
	}
	
	function home(){
		$bobot = $this->NilaiModel->get_bobot()->num_rows();
		if($bobot < 1){
			echo "<script>window.alert('Belum ada data, silahkan generate data terlebih dahulu di menu kriteria'); window.location.href='".site_url('Kriteria/home')."' </script>";
			
		} else {
			$data["main_content"] = "v_nilai";
			$data["resultb"] = $this->NilaiModel->get_bobot()->result_array();
			$data["resultk"] = $this->NilaiModel->get_kriteria()->result_array();
			$data["resultt"] = $this->NilaiModel->get_kriteria()->num_rows();
			$data["total"]	 = $this->NilaiModel->get_total()->result_array();
			$this->load->view("includes/template",$data);
		}
		
	}
	
	function save(){
		$cr1 	= htmlspecialchars($this->input->post('cr1'));
		$cr2 	= htmlspecialchars($this->input->post('cr2'));
		$nil 	= htmlspecialchars($this->input->post('nilai'));
		$data 	= array(
					"nilai" => $nil
				);
		$save = $this->NilaiModel->save_data($data,$cr1,$cr2,$nil);
		if($save){
			$response = array("status"=>"berhasil insert nilai");
			echo json_encode($response);
			exit();
		} else {
			$response = array("status"=>"gagal insert nilai");
			echo json_encode($response);
		}
	}
	
	function generate_agen(){
		$cek = $this->db->get("bobot")->num_rows();
		$cek_a = $this->db->get("tmp_agent")->num_rows();
		if($cek < 1){
			$response = array("status" => "Tidak ada data untuk di generate");
			echo json_encode($response);
		} elseif($cek_a > 0) {
			$response = array("status" => "Sudah pernah generate total agent");
			echo json_encode($response);
		} else {
			$this->NilaiModel->gen_total();
			$response = array("status" => "berhasil insert total agent");
			echo json_encode($response);
		}
		
	}
	
	function cek_total(){
		$tmp_agent = $this->NilaiModel->get_total()->num_rows();
		$response = array("tmp" => $tmp_agent);
		echo json_encode($response);
	}
}