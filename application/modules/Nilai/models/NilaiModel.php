<?php

class NilaiModel extends CI_Model{
	
	function get_bobot(){
		return $this->db->get('bobot');
	}
	function get_kriteria(){
		return $this->db->get('kriteria');
	}
	function gen_total(){
		$noUrut = 1;
		$char = 'CR';
		$kodebaris = $char . sprintf("%03s", $noUrut);
		//
		$noagen = 1;
		$char = 'AR';
		$kodea = $char . sprintf("%03s", $noagen);
		$this->db->select('id');
		$kriteria = $this->db->get('kriteria')->result_array();
		$kolom = $this->db->get('kriteria')->num_rows();
		$data = array();
		foreach($kriteria as $row){
			array_push($data,$row["id"]);
		}
		for($i=1;$i<=$kolom;$i++){
			$this->db->select_sum("nilai");
			$this->db->where_in("baris",$data);
			$this->db->where("kolom",$kodebaris);
			$tot = $this->db->get("bobot")->row();
			$data2 = array("id"=>$kodea,"nilai"=>$tot->nilai);
			$this->db->insert("tmp_agent",$data2);
			$kodebaris++;
			$kodea++;
		}					
	}
	function save_data($data,$cr1,$cr2,$nil){
		$this->db->where(array("baris"=>$cr1,"kolom"=>$cr2));
		$update = $this->db->update("bobot",$data);
		if($update){
			$nilai2 = 1 / $nil;
			$this->db->where(array("baris"=>$cr2,"kolom"=>$cr1));
			$data2 = array("nilai"=>$nilai2);
			$this->db->update("bobot",$data2);
		}
		return true;
	}
	
	function get_total(){
		return $this->db->get("tmp_agent");
	}
	
}